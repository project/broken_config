### Why
Drupal makes it possible to update configuration in different ways.
The ConfigFactory::getEditable allows editing any configuration type.
It also allows to mess up and bad -
for example, assuming that some entity configuration exists and partially
updating creates an empty config file that can't be loaded because required
values are missing.
It results in a broken admin side - either the toolbar link can't be loaded
(like the view configuration), or some admin screen related to that config
fails to load. When that happens, it is hard to find the config which is broken.

### What the module does
The module currently scans all config entity types and queries all entity IDs
from the config table, which are loaded one by one. If the loading fails, we can
assume it's broken and can be either deleted, re-imported, or manually fixed
with Drush config commands.

Basically it is a simple helper module to quickly find possibly broken
configurations.

### How to use it

Open administration url /admin/config/development/configuration/broken
Click on the Start scanning button
After batch job is finished, all problematic configs are shown.

If administration is not accessible (fatal error), then Drush command
could help:
````bash
drush broken_config:scan
````
If problematic configs are found, then it is possible to delete them with

````bash
drush cdel config_name
````

Alternatively, `hook_update_N()` can be used to fix it up.
