<?php

namespace Drupal\broken_config;

/**
 * Batch processing helper class.
 */
class BrokenConfigBatch {

  /**
   * Batch task which processes specific entity type.
   *
   * @param string $entity_type
   *   Entity type to process.
   * @param array $context
   *   Context.
   */
  public static function scanConfigEntityType($entity_type, array &$context): void {
    $message = "Scanning configuration entity type $entity_type";

    \Drupal::service('broken_config.scanner')->scanEntityType($entity_type);

    $context['message'] = $message;
    $context['results'][] = '';
  }

  /**
   * Batch finished callback.
   *
   * @param string $success
   *   Batch job status.
   * @param string $results
   *   Total entity types processed.
   */
  public static function scanFinished($success, $results): void {
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        count($results),
        'One config entity processed.', '@count config entities processed.'
      );
    }
    else {
      $message = t('Error scanning entities.');
    }
    \Drupal::state()->set('broken_config.last_scan', \Drupal::time()->getRequestTime());
    \Drupal::messenger()->addMessage($message);
  }

}
