<?php

namespace Drupal\broken_config\Controller;

use Drupal\broken_config\BrokenConfigScanner;
use Drupal\broken_config\Form\ScanConfigForm;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller.
 */
class BrokenConfigController extends ControllerBase {

  /**
   * Broken config scanner.
   *
   * @var \Drupal\broken_config\BrokenConfigScanner
   */
  protected BrokenConfigScanner $brokenConfigScanner;

  /**
   * Constructor.
   *
   * @param \Drupal\broken_config\BrokenConfigScanner $brokenConfigScanner
   *   Broken config scanner.
   */
  public function __construct(BrokenConfigScanner $brokenConfigScanner) {
    $this->brokenConfigScanner = $brokenConfigScanner;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('broken_config.scanner')
    );
  }

  /**
   * Broken config report page.
   *
   * @return array
   *   Render array.
   */
  public function report(): array {
    $build = [];
    $last_scan = $this->brokenConfigScanner->getLastScan();
    $rows = [];

    foreach ($this->brokenConfigScanner->getBrokenConfiguration() as $entity_type => $values) {
      foreach ($values as $entity_id) {
        $rows[] = [
          $entity_type,
          $entity_id,
          'Delete',
        ];
      }
    }

    $header = [
      $this->t('Config type'),
      $this->t('Config id'),
      $this->t('Action'),
    ];

    $build['report'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $last_scan ? $this->t('Configuration seems to be fine.') : $this->t('Configuration scan is not yet performed'),
    ];

    $build['scan_form'] = $this->formBuilder()->getForm(ScanConfigForm::class);

    return $build;

  }

}
