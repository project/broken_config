<?php

namespace Drupal\broken_config\Form;

use Drupal\broken_config\BrokenConfigScanner;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Config scanner form.
 */
class ScanConfigForm extends FormBase {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Broken configuration scanner.
   *
   * @var \Drupal\broken_config\BrokenConfigScanner
   */
  protected BrokenConfigScanner $brokenConfigScanner;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\broken_config\BrokenConfigScanner $brokenConfigScanner
   *   Broken config scanner.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, BrokenConfigScanner $brokenConfigScanner) {
    $this->entityTypeManager = $entityTypeManager;
    $this->brokenConfigScanner = $brokenConfigScanner;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('broken_config.scanner')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'scan_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['actions']['scan'] = [
      '#type' => 'submit',
      '#value' => $this->t('Start scanning'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $batch = [
      'title' => $this->t('Processing configuration'),
      'operations' => [],
      'finished' => '\Drupal\broken_config\BrokenConfigBatch::scanFinished',
    ];

    $config_entities = $this->brokenConfigScanner->getAllConfigEntityTypes();

    foreach ($config_entities as $config_entity) {
      $batch['operations'][] = [
        '\Drupal\broken_config\BrokenConfigBatch::scanConfigEntityType',
        [$config_entity->id()],
      ];
    }

    batch_set($batch);
  }

}
