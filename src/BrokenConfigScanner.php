<?php

namespace Drupal\broken_config;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Config\Entity\ConfigEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\State\StateInterface;

/**
 * Broken config scanner service.
 */
class BrokenConfigScanner {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * State service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * Logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\State\StateInterface $state
   *   State service.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   Logger channel.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, StateInterface $state, LoggerChannelInterface $logger) {
    $this->entityTypeManager = $entityTypeManager;
    $this->state = $state;
    $this->logger = $logger;
  }

  /**
   * Scan entity type.
   *
   * Loads all configuration entities of specified entity type and tries to load
   * them one by one to find the ones that can't be loaded.
   *
   * @param string $entity_type
   *   Config entity type id to scan.
   */
  public function scanEntityType($entity_type): void {
    try {
      $query = $this->entityTypeManager->getStorage($entity_type)->getQuery();
      $query->accessCheck(FALSE);
      $all_entities = $query->execute();
    }
    catch (PluginException $e) {
      $this->logger->error("Exception thrown in scan process with $entity_type. Exception: {$e->getMessage()}");
      $all_entities = [];
    }

    $broken = [];

    foreach ($all_entities as $entity_id) {
      try {
        $entity = $this->entityTypeManager->getStorage($entity_type)->load($entity_id);
      }
      catch (PluginException $e) {
        $this->logger->error("Exception thrown in scan process with $entity_type. Exception: {$e->getMessage()}");
        $entity = NULL;
      }

      if ($entity === NULL) {
        $broken[] = $entity_id;
      }
    }

    if (!empty($broken)) {
      // Save broken config info into state.
      $this->state->set('broken_config.' . $entity_type, implode(', ', $broken));
    }
    else {
      // Clean up state, if nothing broken was found.
      $this->state->delete('broken_config.' . $entity_type);
    }
  }

  /**
   * Load all config entity types.
   *
   * @return array|ConfigEntityTypeInterface[]
   *   Returns list of all available config entity type definitions.
   */
  public function getAllConfigEntityTypes(): array {
    $definitions = $this->entityTypeManager->getDefinitions();
    $configDefinitions = [];

    foreach ($definitions as $definition) {
      if ($definition instanceof ConfigEntityTypeInterface) {
        $configDefinitions[] = $definition;
      }
    }

    return $configDefinitions;
  }

  /**
   * Get broken config information from state.
   *
   * @return array
   *   Returns list of entity IDs from state, keyed by entity type.
   */
  public function getBrokenConfiguration(): array {
    $entity_types = $this->getAllConfigEntityTypes();
    $configs = [];

    foreach ($entity_types as $entity_type) {
      $value = $this->state->get('broken_config.' . $entity_type->id());

      if (empty($value)) {
        continue;
      }

      $values = explode(', ', $value);

      foreach ($values as $entity_id) {
        $prefix = $entity_type->getConfigPrefix();
        $configs[$entity_type->id()][] = $prefix . '.' . $entity_id;
      }
    }

    return $configs;
  }

  /**
   * Get last scan information.
   *
   * @return null|string
   *   Returns last scan time or NULL if scan is not executed yet.
   */
  public function getLastScan(): ?string {
    return $this->state->get('broken_config.last_scan');
  }

}
