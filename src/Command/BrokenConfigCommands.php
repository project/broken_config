<?php

namespace Drupal\broken_config\Command;

use Drupal\broken_config\BrokenConfigScanner;
use Drush\Commands\DrushCommands;
use Symfony\Component\Console\Helper\Table;

/**
 * Drush commands.
 */
class BrokenConfigCommands extends DrushCommands {

  /**
   * Broken config scanner.
   *
   * @var \Drupal\broken_config\BrokenConfigScanner
   */
  protected BrokenConfigScanner $brokenConfigScanner;

  /**
   * Constructor.
   *
   * @param \Drupal\broken_config\BrokenConfigScanner $brokenConfigScanner
   *   Broken config scanner.
   */
  public function __construct(BrokenConfigScanner $brokenConfigScanner) {
    parent::__construct();

    $this->brokenConfigScanner = $brokenConfigScanner;
  }

  /**
   * Scan for broken config.
   *
   * @command broken_config:scan
   */
  public function scanConfig(): void {
    $rows = [];

    foreach ($this->brokenConfigScanner->getBrokenConfiguration() as $entity_type => $values) {
      foreach ($values as $entity_id) {
        $rows[] = [
          $entity_type,
          $entity_id,
        ];
      }
    }

    if (empty($rows)) {
      $this->logger()?->success('No issues found.');
    }
    else {
      $this->logger()?->warning('Found errors in configuration! Details below');
    }

    $table = new Table($this->output());
    $table->setHeaders(['Config type', 'Config id']);
    $table->addRows($rows);
    $table->render();

    if (!empty($rows)) {
      $this->logger()?->warning('Use Drush to delete broken config files or modify them to fix.');
    }
  }

}
